﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace DTO
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Login { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string Lastname { get; set; }

        public string Firstname { get; set; }

        public string Avatar { get; set; }

        public DepartmentDto Department { get; set; }

        public RoleDto Role { get; set; }
        public Guid? DepartmentId { get; set; }

        public Guid? RoleId { get; set; }
        
    }
}
