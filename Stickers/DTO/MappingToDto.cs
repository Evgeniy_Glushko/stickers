﻿using EmitMapper;

namespace DTO
{
    public static class MappingToDto<TDto, TEntity>

    {
        public static TDto MappingDtoToEntity(TEntity from)
        {
            var mapper = ObjectMapperManager.DefaultInstance.GetMapper<TEntity, TDto>();
            return mapper.Map(from);
        }

    }
}

