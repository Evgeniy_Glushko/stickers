﻿using System;
using System.Linq;
using System.Windows;
using BusinessLayer;
using Catel.Data;
using DTO;

namespace StickersWpf.ViewModels
{
    using Catel.MVVM;
    using System.Threading.Tasks;

    public class UserProfileViewModel : ViewModelBase
    {
        private Manager _mng;
        private UserDto _currentUser;

        public override string Title { get { return "View model title"; } }

        public UserProfileViewModel(UserDto user)
        {
            _mng = new Manager();
            _currentUser = user;
            OnLoadProfileDataCommandExecute();
            SaveProfileCommand = new Command(OnSaveProfileExecute);
            LoadProfileDataCommand = new Command(OnLoadProfileDataCommandExecute);
        }

        public string Login{get { return GetValue<string>(LoginProperty); }set { SetValue(LoginProperty, value); }}
        public static readonly PropertyData LoginProperty = RegisterProperty("Login", typeof(string), null);

        public string Email{get { return GetValue<string>(EmailProperty); }set { SetValue(EmailProperty, value); }}
        public static readonly PropertyData EmailProperty = RegisterProperty("Email", typeof(string), null);

        public string Firstname{get { return GetValue<string>(FirstnameProperty); }set { SetValue(FirstnameProperty, value); }}
        public static readonly PropertyData FirstnameProperty = RegisterProperty("Firstname", typeof(string), null);

        public string Lastname{get { return GetValue<string>(LastnameProperty); }set { SetValue(LastnameProperty, value); }}
        public static readonly PropertyData LastnameProperty = RegisterProperty("Lastname", typeof(string), null);

        public string Department
        {
            get { return GetValue<string>(DepartmentProperty); }
            set { SetValue(DepartmentProperty, value); }
        }
        public static readonly PropertyData DepartmentProperty = RegisterProperty("Department", typeof(string), null);

        public string Role
        {
            get { return GetValue<string>(RoleProperty); }
            set { SetValue(RoleProperty, value); }
        }
        public static readonly PropertyData RoleProperty = RegisterProperty("Role", typeof(string), null);
      
        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();
            // TODO: subscribe to events here
        }

        protected override async Task CloseAsync()
        {
            // TODO: unsubscribe from events here
            await base.CloseAsync();
        }

        public Command SaveProfileCommand { get; private set; }
        private void OnSaveProfileExecute()
        {
            _currentUser.Email = Email;
            _currentUser.Firstname = Firstname;
            _currentUser.Lastname = Lastname;
            _mng.Update(_currentUser);
            MessageBox.Show("Profile has changed");
        }

        public Command LoadProfileDataCommand { get; private set; }
        private void OnLoadProfileDataCommandExecute()
        {
            Login = _currentUser.Login;
            Email = _currentUser.Email;
            Firstname = _currentUser.Firstname;
            Lastname = _currentUser.Lastname;
            var tmpDepartment = _currentUser.Department;
            if (tmpDepartment != null) Department = tmpDepartment.Name;
            var tmpRole = _currentUser.Role;
            if (tmpRole != null) Role = tmpRole.Name;
        }
    }
}
