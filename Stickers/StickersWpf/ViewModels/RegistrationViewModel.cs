﻿using BusinessLayer;
using Catel.Data;
using DTO;
using Catel.MVVM;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Windows;

namespace StickersWpf.ViewModels
{
    public class RegistrationViewModel : ViewModelBase
    {
        Manager _mng;
        public Command RegistrationUserCommand { get; private set; }

        // Конструктор
        public RegistrationViewModel()
        {
            RegistrationUserCommand = new Command(OnRegistrationUserCommandExecute);
            _mng = new Manager();

            // load name Department in comboBox
            ListDepartment = _mng.GetAllDepartaments().Select(a => a.Name).ToArray();  
            ListRole = _mng.GetAllRoles().Select(a => a.Name).ToArray();
        }

        public override string Title { get { return "View model title"; } }

        // TODO: Register models with the vmpropmodel codesnippet
        // TODO: Register view model properties with the vmprop or vmpropviewmodeltomodel codesnippets
        // TODO: Register commands with the vmcommand or vmcommandwithcanexecute codesnippets

        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();
            // TODO: subscribe to events here
        }

        protected override async Task CloseAsync()
        {
            // TODO: unsubscribe from events here
            await base.CloseAsync();
        }

        public string Login
        {
            get { return GetValue<string>(LoginProperty); }
            set { SetValue(LoginProperty, value); }
        }
        public static readonly PropertyData LoginProperty = RegisterProperty("Login", typeof(string), null);

        public string Password
        {
            get { return GetValue<string>(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }
        public static readonly PropertyData PasswordProperty = RegisterProperty("Password", typeof(string), null);

        public string Email
        {
            get { return GetValue<string>(EmailProperty); }
            set { SetValue(EmailProperty, value); }
        }
        public static readonly PropertyData EmailProperty = RegisterProperty("Email", typeof(string), null);

        public string LastName
        {
            get { return GetValue<string>(LastNameProperty); }
            set { SetValue(LastNameProperty, value); }
        }
        public static readonly PropertyData LastNameProperty = RegisterProperty("LastName", typeof(string), null);

        public string FirstName
        {
            get { return GetValue<string>(FirstNameProperty); }
            set { SetValue(FirstNameProperty, value); }
        }
        public static readonly PropertyData FirstNameProperty = RegisterProperty("FirstName", typeof(string), null);

        public string Department
        {
            get { return GetValue<string>(DepartmentProperty); }
            set { SetValue(DepartmentProperty, value); }
        }
        public static readonly PropertyData DepartmentProperty = RegisterProperty("Department", typeof(string), null);

        public string[] ListDepartment
        {
            get { return GetValue<string[]>(ListDepartmentProperty); }
            set { SetValue(ListDepartmentProperty, value); }
        }
        public static readonly PropertyData ListDepartmentProperty = RegisterProperty("ListDepartment", typeof(string[]), null);

        public string Role
        {
            get { return GetValue<string>(RoleProperty); }
            set { SetValue(RoleProperty, value); }
        }
        public static readonly PropertyData RoleProperty = RegisterProperty("Role", typeof(string), null);

        public string[] ListRole
        {
            get { return GetValue<string[]>(ListRoleProperty); }
            set { SetValue(ListRoleProperty, value); }
        }
        public static readonly PropertyData ListRoleProperty = RegisterProperty("ListRole", typeof(string[]), null);


        private void OnRegistrationUserCommandExecute()
        {
            var department = _mng.GetAllDepartaments().FirstOrDefault(a => a.Name == Department);
            var role = _mng.GetAllRoles().FirstOrDefault(a => a.Name == Role);
            var user = new UserDto()
            {
                Id = Guid.NewGuid(),
                Login = Login,
                Email = Email,
                Password = Password,
                Lastname = LastName,
                Firstname = FirstName
            };
            if (department != null)
            {
                user.DepartmentId = department.Id;
            }
            if (role != null)
            {
                user.RoleId = role.Id;
            }

            _mng.Add(user);
            MessageBox.Show("User has added");
        }

    }
}
