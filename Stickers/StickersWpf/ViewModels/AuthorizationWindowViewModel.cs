﻿using System;
using System.Windows;
using BusinessLayer;
using Catel.Data;
using Catel.Services;
using DTO;

namespace StickersWpf.ViewModels
{
    using Catel.MVVM;
    using System.Threading.Tasks;

    public class AuthorizationWindowViewModel : ViewModelBase
    {
        private readonly IUIVisualizerService _visualizerService;
        private Manager _mng;
        public AuthorizationWindowViewModel(IUIVisualizerService visualizerService)
        {
            _mng = new Manager();
            _visualizerService = visualizerService;
            SignInCommand = new Command(OnSignInCommandExecute);
            SignInLikeAdminTestCommand = new Command(OnSignInLikeAdminTestCommandExecute);
        }

        public override string Title { get { return "Sign in to Stickers"; } }

        /// <summary>
        /// Gets or sets the property value. ///
        /// </summary>
        public Guid UserId
        {
            get { return GetValue<Guid>(UserIdProperty); }
            set { SetValue(UserIdProperty, value); }
        }

        /// <summary>
        /// Register the name property so it is known in the class.
        /// </summary>
        public static readonly PropertyData UserIdProperty = RegisterProperty("UserId", typeof(Guid), null);
        /// <summary>
        /// Gets or sets the property value.
        /// </summary>
        public string Password
        {
            get { return GetValue<string>(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        /// <summary>
        /// Register the Password property so it is known in the class.
        /// </summary>
        public static readonly PropertyData PasswordProperty = RegisterProperty("Password", typeof(string), null);
        /// <summary>
        /// Gets or sets the property value.
        /// </summary>
        public string Login
        {
            get { return GetValue<string>(LoginProperty); }
            set { SetValue(LoginProperty, value); }
        }

        /// <summary>
        /// Register the Login property so it is known in the class.
        /// </summary>
        public static readonly PropertyData LoginProperty = RegisterProperty("Login", typeof(string), null);

        // TODO: Register models with the vmpropmodel codesnippet
        // TODO: Register view model properties with the vmprop or vmpropviewmodeltomodel codesnippets
        // TODO: Register commands with the vmcommand or vmcommandwithcanexecute codesnippets

        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();
            // TODO: subscribe to events here
        }

        protected override async Task CloseAsync()
        {
            // TODO: unsubscribe from events here
            await base.CloseAsync();
        }
        /// <summary>
        /// Gets the SignInCommand command.
        /// </summary>
        public Command SignInCommand { get; private set; }

        // TODO: Move code below to constructor
        // TODO: Move code above to constructor

        /// <summary>
        /// Method to invoke when the SignInCommand command is executed.
        /// </summary>
        private void OnSignInCommandExecute()
        {
            UserDto userDto = _mng.CheckOut(Login, Password);
            if (userDto != null)
            {
                _visualizerService.ShowDialog(new MainWindowViewModel(userDto));
            }
            else
            {
                MessageBox.Show("Wrong name or password");
            }
        }

        /// <summary>
        /// Gets the SignInLikeAdminTestCommand command.
        /// </summary>
        public Command SignInLikeAdminTestCommand { get; private set; }

        private void OnSignInLikeAdminTestCommandExecute()
        {
            UserDto userDto = _mng.CheckOut("Evgeniy", "33");
            if (userDto != null)
            {
                _visualizerService.ShowDialog(new MainWindowViewModel(userDto));
            }
            else
            {
                MessageBox.Show("Wrong name or password");
            }
        }

    }
}
