﻿using System;
using System.Collections.Generic;
using Catel.Data;
using Catel.Services;
using Catel.Services.Test;
using DTO;

namespace StickersWpf.ViewModels
{
    using Catel.MVVM;
    using System.Threading.Tasks;

    public class MainWindowViewModel : ViewModelBase
    {
        private List<IViewModel> _slides;
        /// <summary>
        /// Gets the ShowProfileCommand command.
        /// </summary>
        public Command ShowProfileCommand { get; private set; }

        public MainWindowViewModel(UserDto user)
        {
            
            _slides = new List<IViewModel>
            {
                new RegistrationViewModel(),
                new SetPasswordViewModel(user),
                new UserProfileViewModel(user)
            };
            CurrentSlide = _slides[2];
            RegistrationCommand = new Command(OnRegistrationCommandExecute);
            SetPasswordCommand = new Command(OnSetPasswordCommandExecute);
            ShowProfileCommand = new Command(OnShowProfileCommandExecute);
        }

        // TODO: Register models with the vmpropmodel codesnippet
        // TODO: Register view model properties with the vmprop or vmpropviewmodeltomodel codesnippets
        // TODO: Register commands with the vmcommand or vmcommandwithcanexecute codesnippets

        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();

            // TODO: subscribe to events here
        }

        protected override async Task CloseAsync()
        {
            // TODO: unsubscribe from events here

            await base.CloseAsync();
        }

        public IViewModel CurrentSlide
        {
            get { return GetValue<IViewModel>(CurrentSlideProperty); }
            set { SetValue(CurrentSlideProperty, value); }
        }
        public static readonly PropertyData CurrentSlideProperty = RegisterProperty("CurrentSlide", typeof(IViewModel), null);

        public Command RegistrationCommand { get; private set; }
        private void OnRegistrationCommandExecute()
        {
            CurrentSlide = _slides[0];
        }

        public Command SetPasswordCommand { get; private set; }
        private void OnSetPasswordCommandExecute()
        {
            CurrentSlide = _slides[1];
        }

        /// <summary>
        /// Method to invoke when the ShowProfileCommand command is executed.
        /// </summary>
        private void OnShowProfileCommandExecute()
        {
            CurrentSlide = _slides[2];
        }
    }
}
