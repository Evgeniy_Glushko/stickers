﻿using BusinessLayer;
using Catel.Data;
using DTO;
using Catel.MVVM;
using System.Threading.Tasks;
using System.Windows;

namespace StickersWpf.ViewModels
{
    public class SetPasswordViewModel : ViewModelBase
    {
        private UserDto _currentUser;

        public string Password{get { return GetValue<string>(PasswordProperty); }set { SetValue(PasswordProperty, value); }}
        public static readonly PropertyData PasswordProperty = RegisterProperty("Password", typeof(string), null);

        public string RepeatPassword{get { return GetValue<string>(RepeatPasswordProperty); }set { SetValue(RepeatPasswordProperty, value); }}
        public static readonly PropertyData RepeatPasswordProperty = RegisterProperty("RepeatPassword", typeof(string), null);

        public SetPasswordViewModel(UserDto user)
        {
            _currentUser = user;
            SetPasswordCommand = new Command(OnSetPasswordCommandExecute);
        }

        public override string Title { get { return "View model title"; } }

        // TODO: Register models with the vmpropmodel codesnippet
        // TODO: Register view model properties with the vmprop or vmpropviewmodeltomodel codesnippets
        // TODO: Register commands with the vmcommand or vmcommandwithcanexecute codesnippets

        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();
            // TODO: subscribe to events here
        }

        protected override async Task CloseAsync()
        {
            // TODO: unsubscribe from events here
            await base.CloseAsync();
        }

        public Command SetPasswordCommand { get; private set; }

        private void OnSetPasswordCommandExecute()
        {
            var _mng = new Manager();
            if (Password.Equals(RepeatPassword))
            {
                _currentUser.Password = Password;
                _mng.Update(_currentUser);
                MessageBox.Show("Password has been changed");
            }
            else
            {
                MessageBox.Show("Fields don't match");
            }
        }
    }
}
