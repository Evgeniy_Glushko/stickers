﻿using System;

namespace StickersWpf.Views
{
    using ViewModels;

    public partial class AuthorizationWindowView
    {
        public AuthorizationWindowView()
            : this(null)
        { }

        public AuthorizationWindowView(AuthorizationWindowViewModel viewModel)
        {
            InitializeComponent();
        }

        private void OnExit(object sender, System.Windows.RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
