﻿using StickersWpf.ViewModels;

namespace StickersWpf.Views
{
    public partial class RegistrationView
    {
        public RegistrationView()
            : this(null)
        { }

        public RegistrationView(RegistrationViewModel viewModel)
        {
            InitializeComponent();
        }
    }
}
