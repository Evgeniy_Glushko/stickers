﻿using DataLayer;
using DTO;
using Interfaces;

namespace BusinessLayer
{
    public partial class Manager
    {
        private static ServiceDAL _srv;
        private static ServiceDAL ServiceDal => _srv;

        public Manager()
        {
            _srv = new ServiceDAL();
        }
    }
}
