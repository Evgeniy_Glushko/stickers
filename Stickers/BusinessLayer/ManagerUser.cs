﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer;
using DTO;

namespace BusinessLayer
{
    public partial class Manager
    {
        public void Add(UserDto user)
        {
            ServiceDal.Users.Add(user);
        }

        public void Remove(UserDto user)
        {
            ServiceDal.Users.Remove(user);
        }

        public void Update(UserDto user)
        {
            ServiceDal.Users.Update(user);
        }

        public List<UserDto> GetAllUsers()
        {
            return ServiceDal.Users.GetList().ToList();
        }

       

        //public DepartmentDto GetDepartment(Guid? id)
        //{
        //    return MappingToDto(ServiceDal.Departments.GetList().Find(x => x.Id == id));
        //}

        //public RoleDto GetRole(Guid? id)
        //{
        //    return MappingToDto(ServiceDal.Roles.GetList().Find(x => x.Id == id));
        //}

        //public List<UserDto> Find(Guid? id)
        //{
        //    return ServiceDal.Users.GetList().Where(x => x != null && x.Id == id).Select(MappingToDto).ToList();
        //}


    }
}
