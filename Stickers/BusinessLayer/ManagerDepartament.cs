﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace BusinessLayer
{
    public partial class Manager
    {
        public List<DepartmentDto> GetAllDepartaments()
        {
            return ServiceDal.Departments.GetList().ToList();
        }
    }
}
