﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace BusinessLayer
{
    public partial class Manager
    {
        public UserDto CheckOut(string login, string password)
        {
            List<UserDto> list = GetAllUsers();
            UserDto user = list.Find(x => x.Login == login && x.Password == password);
            return user;
        }
    }
}
