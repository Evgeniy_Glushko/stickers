﻿using System.Collections.Generic;
using System.Linq;
using DTO;

namespace BusinessLayer
{
    public partial class Manager
    {
        public List<RoleDto> GetAllRoles()
        {
            return ServiceDal.Roles.GetList().ToList();
        }
        

    }
}
