﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IRole
    {
        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        ICollection<IUsers> Users { get; set; }
    }
}
