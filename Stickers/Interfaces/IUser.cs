﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
        public interface IUsers
        {
            Guid Id { get; set; }
            string Login { get; set; }

            string Password { get; set; }

            string Email { get; set; }

            string Lastname { get; set; }

            string Firstname { get; set; }

            string Avatar { get; set; }

           IDepartment Departments { get; set; }

            IRole Roles { get; set; }
    }
}
