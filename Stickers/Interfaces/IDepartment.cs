﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IDepartment
    {
        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        //ICollection<UserDto> Users { get; set; }
    }
}
