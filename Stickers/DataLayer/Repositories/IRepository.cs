﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace DataLayer
{
    public interface IRepository<TEntity, TDto> where TEntity : BaseEntity where TDto : class
    {
        void Add(TDto obj);
        void Remove(TDto obj);
        void Update(TDto obj);
        IEnumerable<TDto> GetList();
    }
}

