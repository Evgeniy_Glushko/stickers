﻿using DTO;
using Interfaces;

namespace DataLayer
{
    public class ServiceDAL
    {
        static ServiceDAL()
        {
          MapperConfig.RegisterMappings();  
        }

        private static IRepository<User, UserDto> _usersRepo;
        private static IRepository<Role, RoleDto> _rolesRepo;
        private static IRepository<Department, DepartmentDto> _departmentsRepo;

        public IRepository<User, UserDto> Users => _usersRepo ?? (_usersRepo = new Repository<User, UserDto>());
        public IRepository<Role, RoleDto> Roles => _rolesRepo ?? (_rolesRepo = new Repository<Role, RoleDto>());
        public IRepository<Department, DepartmentDto> Departments => _departmentsRepo ?? (_departmentsRepo = new Repository<Department, DepartmentDto>());


    }
}
