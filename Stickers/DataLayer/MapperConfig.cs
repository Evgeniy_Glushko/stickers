﻿using AutoMapper;
using DTO;

namespace DataLayer
{
    public class MapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<User, UserDto>();
            Mapper.CreateMap<UserDto, User>();
            Mapper.CreateMap<Role, RoleDto>();
            Mapper.CreateMap<RoleDto, Role>();
            Mapper.CreateMap<Department, DepartmentDto>();
            Mapper.CreateMap<DepartmentDto, Department>();
        }
    }
}
